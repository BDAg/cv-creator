import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompetenciasComponent } from './competencias/competencias.component';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { ExperienciasComponent } from './experiencias/experiencias.component';
import { FimComponent } from './fim/fim.component';
import { FormacaoComponent } from './formacao/formacao.component';
import { HomeComponent } from './home/home.component';
import { IdiomasComponent } from './idiomas/idiomas.component';

const routes: Routes = [
  { path: 'competencias', component: CompetenciasComponent },
  { path: 'experiencias', component: ExperienciasComponent },
  { path: 'formacao', component: FormacaoComponent },
  { path: 'idiomas', component: IdiomasComponent },
  { path: 'dados-pessoais', component: DadosPessoaisComponent },
  { path: 'fim', component: FimComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
