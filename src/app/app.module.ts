import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CompetenciasComponent } from './competencias/competencias.component';
import { ExperienciasComponent } from './experiencias/experiencias.component';
import { FormacaoComponent } from './formacao/formacao.component';
import { IdiomasComponent } from './idiomas/idiomas.component';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { FimComponent } from './fim/fim.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CompetenciasComponent,
    ExperienciasComponent,
    FormacaoComponent,
    IdiomasComponent,
    DadosPessoaisComponent,
    FimComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
